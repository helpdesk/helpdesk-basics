#!/usr/bin/env bash

cd "${BASH_SOURCE%/*}/" || exit

DOCS=$(find ../docs/ -type f -name "*.md" | sort | tr '\n' ' ')

EXPORT_PDF="../export/helpdesk-basics.pdf"
EXPORT_MD="../export/helpdesk-basics.md"

METADATA="../metadata/metadata.yaml"
TOC="../metadata/toc.md"
TITLE="../metadata/title.md"
TEMPLATE="../templates/default.latex"

echo "Generating PDF export..."
pandoc --pdf-engine=xelatex --toc --toc-depth=3 --template $TEMPLATE -o $EXPORT_PDF $METADATA $DOCS

echo "Generating Markdown export..."
pandoc -s -t gfm -o $EXPORT_MD $TITLE $TOC $DOCS
# workaround for GitLab TOC (bypass pandoc escaping)
sed -i -e 's/%TOC%/[[_TOC_]]/' $EXPORT_MD
