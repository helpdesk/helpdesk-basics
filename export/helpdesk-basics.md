# OTRS-Grundlagen

Grundlagenanleitung zur Arbeit mit dem Helpdesk-System *OTRS* an der FAU

-   Dominik Volkamer, [Regionales Rechenzentrum Erlangen
    (RRZE)](https://www.rrze.fau.de)

## Inhaltsverzeichnis

[[_TOC_]]

# Einführung

Das Regionale Rechenzentrum Erlangen (RRZE) nutzt das Ticketsystem
*OTRS* ([RRZE-Helpdesk](https://www.helpdesk.rrze.fau.de)) zur
Kommunikation mit Kunden und bietet die Nutzung des Ticketsystems als
Dienstleistung für Einrichtungen der FAU an. In dieser Anleitung sollen
wichtige Begriffe, die Funktionsweise eines Ticketsystems und dessen
grundlegende Konzepte beschrieben werden. Außerdem werden die Themen
*Persönliche Einstellungen*, *Übersicht* und *Tickets* genauer
behandelt. Diese Anleitung ist sowohl die Grundlage als auch eine
Ergänzung zu einer Erstanwenderschulung für das Ticketsystem OTRS.

# Wichtige Begriffe

## OTRS

-   OTRS ist ein Ticketsystem und dient zur Erfassung, Speicherung und
    Bearbeitung von Kundenanfragen.
-   Die Abkürzung *OTRS* steht für *Open Technology Real Services*
    (früher für *Open Ticket Request System*).
-   OTRS steht als freie Software unter der Lizenz *GNU General Public
    License* (GPL), d. h. der Quelltext ist frei verfügbar und kann
    angepasst und erweitert werden.
-   OTRS ist ein weitverbreitetes System im Unternehmensumfeld und wird
    von vielen großen Unternehmen zur Kundenkommunikation eingesetzt.
-   Das Ticketsystem ist eine Webanwendung, d. h. es wird über den
    Browser (z. B. Firefox, Chrome) bedient und erfordert keine
    Installation für die Nutzer des Systems auf dem PC oder mobilen
    Gerät (plattform- und betriebssystemunabhängig).

## Kunde

-   Ein Kunde ist im Kontext eines Ticketsystems (vereinfacht
    ausgedrückt) der Ersteller einer Anfrage.
-   Der Kunde weiß normalerweise nicht, dass er mit einem Ticketsystem
    korrespondiert.
-   Der Kunde kann sich nicht im Ticketsystem einloggen, ein
    Kundenportal steht für den RRZE-Helpdesk (OTRS) nicht mehr zur
    Verfügung.

## Agent

-   Ein Agent bearbeitet Anfragen (Tickets) und Einträge in der
    Wissensdatenbank (FAQs) im Ticketsystem.
-   Um als Agent mit dem Ticketsystem zu arbeiten, ist ein Login über
    die Weboberfläche am [RRZE-Helpdesk
    (OTRS)](https://www.helpdesk.rrze.fau.de) notwendig.
-   Ein Zugang zum RRZE-Helpdesk (OTRS) wird nicht automatisch vergeben,
    sondern muss gesondert beantragt werden (siehe
    [OTRS-Zugang](https://www.anleitungen.rrze.fau.de/otrs-ticketsystem/otrs-zugang/)).

## Ticket und Artikel

-   Ein Ticket bündelt die gesamte Kommunikation einer Kundenanfrage.
-   Jedes Ticket erhält eine eindeutige Ticketnummer, über die das
    Ticket jederzeit wiedergefunden werden kann.
-   Tickets beinhalten i. d. R. mehrere Artikel, die vom System
    chronologisch durchnummeriert und in der Artikelübersicht eines
    Tickets angezeigt werden.
-   Ein Artikel entspricht einer ein- oder ausgehenden E-Mail oder einem
    Bearbeitungsschritt (z. B. einer Notiz) in einem Ticket.

## Notiz

-   Zur internen Kommunikation zwischen Agenten werden im Ticketsystem
    sogenannte Notizen verwendet.
-   Notizen bieten sich an, um beispielsweise anderen Agenten den
    aktuellen Stand des Tickets mitzuteilen.
-   Viele Ticket-Aktionen hängen automatisch eine Notiz an das Ticket
    an, in der die Aktion und der Grund dieser genauer beschrieben
    werden kann.

## Queue

-   Eine sogenannte Queue ist ein Postfach bzw. eine Warteschlange für
    Tickets abgegrenzter Bereiche.
-   Ein Ticket ist immer genau einer Queue zugeordnet.
-   Queues sind ineinander verschachtelbar, jeweils getrennt durch zwei
    Doppelpunkte (z. B. *RRZE::OTRS-Support*).
-   Tickets können zwischen verschiedenen Queues verschoben werden.
-   Pro Queue können je eine Anrede und eine Signatur sowie mehrere
    Vorlagen und automatische Antworten konfiguriert werden.
-   Berechtigungen für Agenten werden auf Basis von Queues vergeben.
-   Es können eine oder mehrere eingehende E-Mail-Adresse/n
    (Funktionsadresse/n) je Queue konfiguriert werden. Jede Queue hat
    genau eine Absenderadresse (Funktionsadresse).

## Anrede und Signatur

-   Pro Queue ist je eine Anrede und eine Signatur konfigurierbar.
-   Bei jeder Antwort an den Kunden wird die Anrede und Signatur
    automatisch eingefügt und Variablen (z. B. der Name des Kunden, der
    Name des Agenten) eingesetzt.
-   Die Anrede/Signatur einer Queue wird vom OTRS-Support bzw. von einem
    Agent mit der
    [Mini-Admin-Rolle](https://www.anleitungen.rrze.fau.de/otrs-ticketsystem/mini-admin-rolle/)
    (nur Signatur) gepflegt.

## Vorlage und automatische Antwort

-   Eine Vorlage dient zum Antworten auf Tickets des Kunden oder zum
    Erstellen von neuen Telefon- oder E-Mail-Tickets (s. u.).
-   In Vorlagen können beispielsweise häufig verwendete Antworttexte
    hinterlegt und einfach wiederverwendet werden.
-   Eine automatische Antwort dient zur automatischen Benachrichtigung
    des Kunden, z. B. um den Erhalt der Anfrage zu bestätigen.
-   Es gibt verschiedene Typen von automatischen Antworten, z. B. bei
    der Erstellung eines neuen Tickets oder bei einer Nachfrage des
    Kunden.
-   Vorlagen und automatische Antworten werden vom OTRS-Support bzw. von
    einem Agent mit der
    [Mini-Admin-Rolle](https://www.anleitungen.rrze.fau.de/otrs-ticketsystem/mini-admin-rolle/)
    gepflegt.

## Historie

-   OTRS ist revisionssicher, d. h. alle Aktionen an einem Ticket werden
    in einer Historie gespeichert und bleiben nachvollziehbar.
-   Die Historie kann über die Ticket-Aktion *Verschiedenes-Historie* in
    der Ticket-Detail-Ansicht für jedes Ticket aufgerufen werden.
-   Das Löschen von Tickets ist für Agenten nicht möglich (nur über den
    OTRS-Support). Viele Ticket-Aktionen lassen sich nicht rückgängig
    machen oder können nicht nachträglich geändert werden.

## FAQ

-   Der RRZE-Helpdesk (OTRS) stellt das sogenannte FAQ-Modul bereit.
    Hierbei handelt es sich um eine ins Ticketsystem integrierte
    Wissensdatenbank.
-   In der Wissensdatenbank können häufig gestellte Fragen (FAQ =
    Frequently Asked Questions) strukturiert abgelegt werden.
-   Es ist möglich, FAQ-Artikel als *intern* (nur für andere Agenten des
    selben Bereichs) oder als *öffentlich* (weltweit abrufbar) zu
    markieren.

# Funktionsweise und grundlegende Konzepte

## Funktionsweise

### Kommunikation per E-Mail

-   Ein Kunde (z. B. ein Student) hat ein Anliegen oder ein Problem und
    wendet sich per E-Mail an eine Funktionsadresse (z. B. bei einem
    Problem mit dem WLAN an <wlan@fau.de>), die an das Ticketsystem
    zugestellt wird.
-   Aus der E-Mail des Kunden wird ein neues Ticket mit eindeutiger
    Ticket-Nummer im System generiert.
-   Funktionsadressen, die an das Ticketsystem zugestellt werden, sind
    immer mit Queues verknüpft, d. h. das Ticket wird vom System
    automatisch in die passende Queue einsortiert.
-   Alle Agenten mit Zugriff auf die entsprechende Queue (z. B. alle
    Mitarbeiter der Netzwerkabteilung) sehen das neue Ticket des Kunden.
-   Ein Agent verfasst im Ticketsystem eine Antwort auf die Anfrage des
    Kunden. Die Antwort des Agenten wird an das Ticket angehängt und vom
    Ticketsystem per E-Mail an den Kunden gesendet.
-   Falls die Antwort des Agenten das Problem des Kunden nicht sofort
    löst, hat der Kunde die Möglichkeit eine Nachfrage zu stellen. Diese
    Nachfrage wird wieder an das Ursprungsticket angehängt und kann
    erneut von einem Agenten beantwortet werden.

### Kommunikation per Telefon

-   Ein Kunde hat ein Anliegen oder ein Problem und wendet sich
    telefonisch an die Hotline eines Helpdesks.
-   Der Hotline-Mitarbeiter (und gleichzeitig OTRS-Agent) erstellt ein
    neues Ticket im Ticketsystem, in dem er das Anliegen des Kunden
    nachvollziehbar dokumentiert.
-   Nach der Erfassung des Anliegens kann dieses bearbeitet werden und
    eine Antwort an Kunden per E-Mail verfasst werden. Alternativ kann
    der Kunde telefonisch informiert werden und der Agent kann dies im
    Ticket dokumentieren.

## Ticket-Status

-   Einem Ticket ist immer genau ein Ticket-Status zugeordnet.
-   Der Ticket-Status zeigt dem Agenten den aktuellen Stand des Tickets
    an und wirkt sich auf die Sichtbarkeit eines Tickets im Ticketsystem
    aus.
-   Im RRZE-Helpdesk (OTRS) sind folgende Ticket-Status möglich:
    -   **neu:** neues Ticket ohne ausgehende Kommunikation eines
        Agenten an den Kunden (eine automatische Antwort ändert den
        Status nicht)
    -   **offen:** offenes Ticket (muss noch bearbeitet werden)
    -   **erfolgreich geschlossen:** erfolgreich geschlossenes Ticket
        (nur über die Suche auffindbar)
    -   **erfolglos geschlossen:** erfolglos geschlossenes Ticket (nur
        über die Suche auffindbar)
    -   **Spam manuell geschlossen:** spezieller Status für die
        Ticket-Aktion *Spam* (geschlossen und nur über die Suche
        auffindbar)
    -   **zusammengefasst:** zusammengefasstes Ticket (das Ticket wurde
        mit einem anderen Ticket zusammengefasst)
    -   **warten auf erfolglos schließen:** Ticket wartet bis zur
        voreingestellten Zeit und wird dann automatisch erfolglos
        geschlossen
    -   **warten auf erfolgreich schließen:** Ticket wartet bis zur
        voreingestellten Zeit und wird dann automatisch erfolgreich
        geschlossen
    -   **warten zur Erinnerung:** Ticket wartet bis zur
        voreingestellten Zeit und Agent wird an das Ticket erinnert

## Ticket-Sperre

-   Zum Schutz vor gleichzeitiger Bearbeitung eines Tickets gibt es die
    sogenannte Ticket-Sperre.
-   Tickets können entweder *frei* oder *gesperrt* sein.
-   Ist ein Ticket auf einen Agenten gesperrt, ändert sich die
    Sichtbarkeit in der Oberfläche und andere Agenten können nicht mehr
    alle Ticket-Aktionen ausführen.
-   Einige Aktionen sperren das Ticket automatisch (z. B. das Verfassen
    einer Antwort an den Kunden).
    -   Bei einer versehentlicher Ticket-Aktion empfiehlt es sich immer
        auf *Rückgängig machen und Beenden* (oben links im Pop-up) zu
        klicken. Dadurch wird das Ticket wieder für andere Agenten
        freigegeben (Besitzer bleibt erhalten).
-   Manuelles Sperren/Entsperren ist über die Ticket-Aktion *Sperren*
    bzw. *Entsperren* (wechselt je nach Sperre des Tickets) in der
    Ticket-Detail-Ansicht möglich.
-   Ein Ticket wird nach der pro Queue voreingestellten Zeit,
    normalerweise nach ca. 4 Arbeitstagen, automatisch wieder
    freigegeben.

## Besitzer eines Tickets

-   Besitzer eines Tickets ist der Agent, der die letzte Ticket-Aktion
    mit Ticket-Sperre ausgeführt hat.
-   Sobald das Sperren des Tickets für das Ausführen der Ticket-Aktion
    nötig ist, wird der Besitzer automatisch auf den ausführenden Agent
    geändert.
-   Der Besitzer lässt sich im Menü *Personen - Besitzer: Neuer
    Besitzer* in der Ticket-Detail-Ansicht manuell ändern. Das Ticket
    wird dann immer automatisch auch auf den neuen Besitzer gesperrt.
-   Solange das Ticket nicht auf den Besitzer gesperrt ist, gibt es
    keine Einschränkungen für die Bearbeitung - dies kann also im
    Prinzip ignoriert werden.

## Ticket-Aktionen

### Ticket beobachten

-   Mit der Ticket-Aktion *Beobachten* kann ein Agent alle Aktivitäten
    und den Status eines Tickets verfolgen.
-   Mit einem Klick auf die Ticket-Aktion *Beobachten* in der
    Ticket-Detail-Ansicht wird das Ticket in die Beobachtungsliste
    aufgenommen (Symbol *Auge* erscheint).
-   Bei einer neuen Aktion eines anderen Agenten bzw. eines Kunden in
    einem beobachteten Ticket, erscheint oben im Menü das Symbol *Auge
    mit Stern*.
-   Eine Übersicht über alle beobachteten Tickets (*Meine beobachteten
    Tickets*), erhält man durch einen Klick auf das Symbol *Auge*.
-   Zum Aufheben der Beobachtung muss für jedes Ticket die Ticket-Aktion
    *Nicht beobachten* in der Ticket-Detail-Ansicht durchgeführt werden.

### Ticket zusammenfassen

-   Zwei Tickets können mit der Ticket-Aktion *Zusammenfassen* zu einem
    Ticket zusammengefasst werden.
-   Das Zusammenfassen von Tickets bietet sich beispielsweise an, wenn
    ein Kunde ein Problem mehrmals meldet.
-   Das Ursprungsticket bleibt im Ticket-Status *zusammengefasst*
    erhalten, Nachfragen auf das Ursprungsticket werden an das
    zusammengefasste Ticket angehängt.

### Ticket verknüpfen

-   Wenn zwei Tickets in einem Zusammenhang stehen, aber nicht
    zusammengefasst werden können/sollen, ist die Ticket-Aktion
    *Verknüpfen* die richtige Wahl.
-   Das Verknüpfen von Tickets bietet sich beispielsweise an, wenn
    Tickets miteinander verwandt sind, aber nicht zu einem Ticket
    zusammengefasst werden sollen/können.
-   Die Verknüpfung wird in der Ticket-Detail-Ansicht am rechten Rand
    angezeigt.

### Ticket teilen

-   Ein einzelner Artikel kann in ein neues Telefon-Ticket mit der
    Artikel-Aktion *Teilen* geteilt werden.
-   Das Teilen von Tickets bietet sich beispielsweise an, wenn in einem
    Ticket zwei unterschiedliche Probleme beschrieben werden.
-   Die Tickets werden automatisch miteinander verknüpft.

### Ticket verschieben

-   Mit der Ticket-Aktion *Queue* kann ein Ticket in eine andere Queue
    verschoben werden.
-   Das Verschieben von Tickets bietet sich beispielsweise an, wenn das
    Ticket nicht in den Zuständigkeitsbereich der Queue gehört und es
    eine andere passende OTRS-Queue gibt.

## Archivierung von Tickets

-   Im RRZE-Helpdesk (OTRS) werden *erfolgreich/erfolglos geschlossene*
    Tickets, die vor mehr als 3 Monaten (bis zum 30.11.2019: 6 Monaten)
    das letzte Mal geändert wurden, archiviert. Diese Tickets lassen
    sich nur noch über die Archivsuche finden (vgl. dazu FAQ-Artikel:
    [#42001611 Wie finde ich ältere Tickets
    wieder?](https://www.helpdesk.rrze.fau.de/otrs/public.pl?Action=PublicFAQZoom&ItemID=1611)).
-   Die Archivierung dient primär dazu, aktuelle Tickets über die Suche
    schneller zu finden. Eine Archivsuche dauert deutlich länger und
    sollte deshalb, beispielsweise in Suchvorlagen, nicht standardmäßig
    aktiviert werden.
-   Bitte beachten Sie, dass durch die Archivierung das Ticket
    automatisch aus Ihrer Beobachtungsliste entfernt wird (vgl.
    Ticket-Aktionen: Ticket beobachten).

# Persönliche Einstellungen

## Allgemeines

-   OTRS kann durch die persönlichen Einstellungen an die Bedürfnisse
    und Vorlieben des Agenten angepasst werden.
-   [Anzeigen/Ändern der
    Einstellungen](https://www.helpdesk.rrze.fau.de/otrs/index.pl?Action=AgentPreferences)
    ist über das Zahnrad-Symbol im oberen Menü (je nach Skin links oder
    rechts) möglich.

## Mögliche Einstellungen

-   **Sprache:** Sprache der Benutzeroberfläche ändern
-   **Abwesenheitszeit:** Abwesenheitszeiten pflegen
-   **Persönlicher Namenszusatz (Adelstitel):** Persönlichen
    Namenszusatz pflegen (i. d. R. irrelevant)
-   **Meine Queues:** Queue(s) abonnieren für Ticket-Übersichten und
    E-Mail-Benachrichtigungen
-   **Ticket-Benachrichtigungen:** E-Mail-Benachrichtigungen
    ein-/ausschalten (bezieht sich immer auf alle unter *Meine Queues*
    abonnierten Queues)
-   **Skin:** Oberflächen-Skin ändern
-   **Aktualisierungszeiten der Übersichten:** Automatisches Neuladen
    bestimmter Seiten
-   **Ansicht nach Ticket-Erstellung:** Verhalten nach der
    Ticket-Erstellung

# Übersicht (Dashboard)

## Allgemeines

-   Die sogenannte *Übersicht* besteht aus dem Hauptbereich und einer
    seitlichen Spalte (rechts).
-   Die einzelnen Kästen in der Übersicht werden *Widgets* genannt und
    verfügen teilweise über eigene Einstellungen.
-   Die genannten Bereiche der Übersicht können mit Widgets gefüllt und
    an persönliche Vorlieben angepasst werden.
-   Zum Verschieben der Widgets kann Drag & Drop verwenden werden.
-   Widget entfernen und Widget-Einstellungen (falls möglich):
    -   Die Maus auf die graue Kopfzeile mit der Überschrift eines
        Widgets bewegen.
    -   Es erscheint ein Zahnrad-Symbol für Einstellungen und ein
        *X*-Symbol zum Entfernen des Widgets
-   Gelöschte Widgets können jederzeit über den Menüpunkt
    *Einstellungen* oben rechts in der seitlichen Spalte wieder in die
    Übersicht geholt werden.
-   Die Reihenfolge der Menüpunkte (z. B. Übersicht, Tickets, FAQ, etc.)
    im oberen Menü kann ebenfalls durch Drag & Drop angepasst werden.

## Widgets in der Übersicht

-   Durch Klick auf den Spaltenkopf in Widgets mit Ticket-Übersichten
    (z. B. *Neue Tickets*) kann die Sortierung angepasst werden.
-   Reiter in den Widgets beachten:
    -   **Meine gesperrten Tickets:** auf mich gesperrte Tickets
    -   **Meine beobachteten Tickets:** von mir beobachtete Tickets
    -   **Tickets in “Meine Queues”:** Tickets in meinen abonnierten
        Queues (siehe *Persönliche Einstellungen*)
    -   **Alle Tickets:** Tickets aller Queues (auf die ich
        Berechtigungen habe)
-   Über das Trichter-Icon können Tickets nach bestimmten Kriterien in
    Widgets mit Ticket-Übersichten gefiltert werden (Achtung:
    Einstellung wird gespeichert).
-   Wenn in Widgets mit Ticket-Übersichten zu viele Tickets vorhanden
    sind, werden diese auf mehreren Seiten (Pagination) angezeigt. Ein
    Seitenwechsel ist über die Seitenzahl bzw. Pfeilsymbole oben rechts
    möglich.

# Tickets

## Neues Telefon-Ticket

-   Ein Agent erstellt ein neues Telefon-Ticket, wenn er von einem
    Kunden angerufen wird und den Inhalt des Gesprächs in einem Ticket
    dokumentieren will.
-   Der Kunde wird normalerweise nicht über das Erstellen des Tickets
    informiert (nur bei einer konfigurierten automatischen Antwort in
    der entsprechenden Queue).
-   Zum Erstellen eines neuen Telefon-Tickets müssen mindestens die
    Pflichtfelder des Formulars ausgefüllt werden. Ein Klick auf
    *Erstellen* erzeugt das neue Ticket.

## Neues E-Mail-Ticket

-   Ein Agent erstellt ein neues E-Mail-Ticket, wenn die Anfrage vom
    Agenten und nicht vom Kunden ausgeht und die Antwort des Kunden im
    Ticketsystem gespeichert werden soll.
-   Bei einem neuen E-Mail-Ticket bekommt der Kunde immer eine E-Mail.
-   Zum Erstellen eines neuen E-Mail-Tickets müssen mindestens die
    Pflichtfelder des Formulars ausgefüllt werden. Ein Klick auf *E-Mail
    übermitteln* erzeugt das neue Ticket.

## Ansicht nach Queues

-   Standardmäßig werden Tickets aus allen abonnierten Queues angezeigt
    (*Meine Queues*, siehe *Persönliche Einstellungen*).
-   Durch Klick auf eine Queue (übergeordnete Queue klappt sich aus und
    zeigt untergeordnete Queues an), kann eine spezielle Queue
    ausgewählt werden.
-   Reiter beachten:
    -   **Alle Tickets:** alle Tickets (inkl. auf andere Agenten
        gesperrte Tickets)
    -   **Verfügbare Tickets:** nicht gesperrte Tickets (Standard)
-   verschiedene Ansichten (Buchstaben-Symbole rechts oben) mit mehr
    oder weniger Information:
    -   **S:** Tabellarische Form (Spalten über Zahnrad-Icon anpassbar)
    -   **M:** Ausführlichere Form mit mehr Ticket-Meta-Informationen
    -   **L:** Ausführlichste Form mit Ticket-Inhalten

## Ticket-Detail-Ansicht

-   Die Ticket-Detail-Ansicht zeigt alle Meta-Informationen und Inhalte
    eines Tickets an.
-   Die Überschrift zeigt die Priorität (farbiges Rechteck), die
    eindeutige Ticket-Nummer und den Ticket-Titel.

### Ticket-Aktionen

-   **Zurück:** zur Übersicht zurückkehren
-   **Sperren/Entsperren:** Ticket auf mich sperren bzw. für alle
    freigeben
-   **Drucken:** PDF-Export zum Ausdrucken
-   **Priorität:** Ticket-Priorität ändern
-   **Personen:**
    -   **Besitzer:** Ticket-Besitzer ändern, d. h. Ticket auf anderen
        Agenten sperren
    -   **Kunde:** Kundenbenutzer ändern
-   **Kommunikation:**
    -   **Notiz:** Notiz an Ticket anhängen
    -   **Ausgehender Telefonanruf:** Notiz über ausgehenden
        Telefonanruf an Ticket anhängen
    -   **Eingehender Telefonanruf:** Notiz über eingehenden
        Telefonanruf an Ticket anhängen
-   **Warten:** Ticket in den Wartezustand versetzen
-   **Beobachten:** Ticket beobachten
-   **Schließen:** Ticket schließen
-   **Spam:** Ticket in die Spam-Queue verschieben und schließen
-   **Verschiedenes:**
    -   **Historie:** Historie eines Tickets anzeigen (alle
        Bearbeitungsschritte)
    -   **Freie Felder:** dynamische Felder hinzufügen/ändern und
        Ticket-Status (z. B. offen, geschlossen) manuell ändern
    -   **Verknüpfen:** Ticket mit anderem Ticket verknüpfen
    -   **Zusammenfassen:** das aktuelle Ticket zu einem anderen Ticket
        (Ticketnummer benötigt) zusammenfassen
    -   **MasterSlave:** MasterSlave-Ticket erstellen (wird eher selten
        benötigt)
-   **Queue:** Ticket in andere Queue verschieben

### Artikelübersicht

-   In der Artikelübersicht werden alle Artikel (z. B. ein- und
    ausgehenden Nachrichten, Notizen) aufgelistet.
-   Mit einem Klick auf einen Artikel in der Artikelübersicht springt
    man zum entsprechenden Artikel.
-   Modus der Ansicht kann über die zwei Strich-Symbole gewechselt
    werden:
    -   **einen Artikel anzeigen:** Es wird immer nur der in der
        Artikelübersicht ausgewählte Artikel angezeigt.
    -   **alle Artikel anzeigen:** Es werden alle Artikel angezeigt und
        durch Klicken auf einen Artikel in der Artikelübersicht scrollt
        man zu entsprechenden Artikel.
-   Über das Trichter-Symbol können bestimmte Artikel gefiltert werden.

### Artikelaktionen

-   **Markieren:** Artikel in der Artikelübersicht hervorheben
-   **Unformatierte Ansicht:** zeigt den Quelltext der E-Mail an (nur
    bei ein- und ausgehenden E-Mails)
-   **Drucken:** PDF-Export zum Ausdrucken eines einzelnen Artikels
-   **Teilen:** neues Ticket mit diesem Artikel erstellen
-   **Umleiten:** leitet den Artikel um (Bounce)
-   **Weiterleiten:** leitet den Artikel weiter (Forward)
-   **Antworten:** Antwort auf einen Artikel verfassen (Auswahl von
    Vorlage über Drop-Down-Menü)

### Randspalte

-   **Ticket-Informationen:** alle Metadaten zu einem Ticket
-   **Kundeninformation:** Informationen zum Kunden eines Tickets
    -   Kundeninformationen werden aus dem IdentityManagement (IdM) der
        FAU bezogen.
    -   Die Anzeige der Kundeninformation funktioniert nur dann, wenn
        der Kunde im IdM mit einer Person an der FAU verknüpft werden
        kann (z. B. Kunde sendet eine Anfrage von seiner
        “@fau.de”-E-Mail-Adresse).
-   **Verknüpfte Objekte:** verknüpfte Tickets oder FAQ-Artikel

## Suchfunktion

-   Geschlossene Tickets sind nur über die Suchfunktion (oder die URL)
    in der Oberfläche auffindbar.
-   Zum Öffnen des Suchformulars auf das *Lupe*-Symbol im oberen Menü
    klicken.
-   Es sind viele verschiedene Suchparameter möglich, z. B.
    Ticket-Nummer, Queue, Volltext.
-   Je genauer die Suche eingegrenzt wird, desto schneller werden
    Ergebnisse gefunden.
-   Erweitern der Suchparameter ist über *Ein weiteres Attribut
    hinzufügen* und einem Klick auf das *Plus*-Symbol möglich.
-   Die Archivsuche ist für Tickets, die vor 6 Monaten oder länger das
    letzte Mal bearbeitet wurden (Achtung: Suche dauert deutlich
    länger).
-   Das Speichern von Suchparametern und Werten für häufig ähnliche
    Suchen ist über Suchvorlagen möglich.
    -   Zunächst alle Suchparameter und Werte wie gewünscht
        konfigurieren.
    -   Anschließend auf *Suchvorlage-Neue anlegen* klicken, Namen für
        die Suche vergeben und *Hinzufügen* klicken.
-   In Ergebnisliste der Suche lassen sich die Suchparameter und Werte
    über *Suchoptionen ändern* nachträglich anpassen (nicht über das
    *Lupe*-Symbol).

# Hinweise zum Dokument

Die Anleitung wurde im Rahmen von Erstanwenderschulungen für das
Helpdesk-System an der FAU im Auftrag des Regionalen Rechenzentrums
Erlangen (RRZE) erstellt.

## Autor

-   Dominik Volkamer, [Regionales Rechenzentrum Erlangen
    (RRZE)](https://www.rrze.fau.de)

## Lizenz

Die Anleitung wird unter einer [Creativ Commons
Lizenz](https://creativecommons.org/licenses/?lang=de) bereitgestellt:

[CC BY-NC-SA
4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de)

-   Namensnennung
-   Nicht kommerziell
-   Weitergabe unter gleichen Bedingungen

## Verwaltung und Bearbeitung der Anleitung

Die Anleitung wird in einem öffentlichen Repository auf der Plattform
*GitLab* des Regionalen Rechenzentrum Erlangen (RRZE) bereitgestellt und
gepflegt:

GitLab-URL: https://gitlab.rrze.fau.de/helpdesk/helpdesk-basics

## Fehler und Ergänzungen

Korrekturvorschläge können dem Autor gerne per
[E-Mail](mailto:dominik.volkamer@fau.de) mitgeteilt werden. Ergänzungen
werden, soweit dies zeitlich möglich ist, auf Wunsch umgesetzt.
