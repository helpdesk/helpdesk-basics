# Funktionsweise und grundlegende Konzepte #

## Funktionsweise ##

### Kommunikation per E-Mail ###

- Ein Kunde (z. B. ein Student) hat ein Anliegen oder ein Problem und wendet sich per E-Mail an eine Funktionsadresse (z. B. bei einem Problem mit dem WLAN an <wlan@fau.de>), die an das Ticketsystem zugestellt wird.
- Aus der E-Mail des Kunden wird ein neues Ticket mit eindeutiger Ticket-Nummer im System generiert.
- Funktionsadressen, die an das Ticketsystem zugestellt werden, sind immer mit Queues verknüpft, d. h. das Ticket wird vom System automatisch in die passende Queue einsortiert.
- Alle Agenten mit Zugriff auf die entsprechende Queue (z. B. alle Mitarbeiter der Netzwerkabteilung) sehen das neue Ticket des Kunden.
- Ein Agent verfasst im Ticketsystem eine Antwort auf die Anfrage des Kunden. Die Antwort des Agenten wird an das Ticket angehängt und vom Ticketsystem per E-Mail an den Kunden gesendet.
- Falls die Antwort des Agenten das Problem des Kunden nicht sofort löst, hat der Kunde die Möglichkeit eine Nachfrage zu stellen. Diese Nachfrage wird wieder an das Ursprungsticket angehängt und kann erneut von einem Agenten beantwortet werden.

### Kommunikation per Telefon ###

- Ein Kunde hat ein Anliegen oder ein Problem und wendet sich telefonisch an die Hotline eines Helpdesks.
- Der Hotline-Mitarbeiter (und gleichzeitig OTRS-Agent) erstellt ein neues Ticket im Ticketsystem, in dem er das Anliegen des Kunden nachvollziehbar dokumentiert.
- Nach der Erfassung des Anliegens kann dieses bearbeitet werden und eine Antwort an Kunden per E-Mail verfasst werden. Alternativ kann der Kunde telefonisch informiert werden und der Agent kann dies im Ticket dokumentieren.

## Ticket-Status ##

- Einem Ticket ist immer genau ein Ticket-Status zugeordnet.
- Der Ticket-Status zeigt dem Agenten den aktuellen Stand des Tickets an und wirkt sich auf die Sichtbarkeit eines Tickets im Ticketsystem aus.
- Im RRZE-Helpdesk (OTRS) sind folgende Ticket-Status möglich:
  - **neu:** neues Ticket ohne ausgehende Kommunikation eines Agenten an den Kunden (eine automatische Antwort ändert den Status nicht)
  - **offen:** offenes Ticket (muss noch bearbeitet werden)
  - **erfolgreich geschlossen:** erfolgreich geschlossenes Ticket (nur über die Suche auffindbar)
  - **erfolglos geschlossen:** erfolglos geschlossenes Ticket (nur über die Suche auffindbar)
  - **Spam manuell geschlossen:** spezieller Status für die Ticket-Aktion *Spam* (geschlossen und nur über die Suche auffindbar)
  - **zusammengefasst:** zusammengefasstes Ticket (das Ticket wurde mit einem anderen Ticket zusammengefasst)
  - **warten auf erfolglos schließen:** Ticket wartet bis zur voreingestellten Zeit und wird dann automatisch erfolglos geschlossen
  - **warten auf erfolgreich schließen:** Ticket wartet bis zur voreingestellten Zeit und wird dann automatisch erfolgreich geschlossen
  - **warten zur Erinnerung:** Ticket wartet bis zur voreingestellten Zeit und Agent wird an das Ticket erinnert

## Ticket-Sperre ##

- Zum Schutz vor gleichzeitiger Bearbeitung eines Tickets gibt es die sogenannte Ticket-Sperre.
- Tickets können entweder *frei* oder *gesperrt* sein.
- Ist ein Ticket auf einen Agenten gesperrt, ändert sich die Sichtbarkeit in der Oberfläche und andere Agenten können nicht mehr alle Ticket-Aktionen ausführen.
- Einige Aktionen sperren das Ticket automatisch (z. B. das Verfassen einer Antwort an den Kunden).
  - Bei einer versehentlicher Ticket-Aktion empfiehlt es sich immer auf *Rückgängig machen und Beenden* (oben links im Pop-up) zu klicken. Dadurch wird das Ticket wieder für andere Agenten freigegeben (Besitzer bleibt erhalten).
- Manuelles Sperren/Entsperren ist über die Ticket-Aktion *Sperren* bzw. *Entsperren* (wechselt je nach Sperre des Tickets) in der Ticket-Detail-Ansicht möglich.
- Ein Ticket wird nach der pro Queue voreingestellten Zeit, normalerweise nach ca. 4 Arbeitstagen, automatisch wieder freigegeben.

## Besitzer eines Tickets ##

- Besitzer eines Tickets ist der Agent, der die letzte Ticket-Aktion mit Ticket-Sperre ausgeführt hat.
- Sobald das Sperren des Tickets für das Ausführen der Ticket-Aktion nötig ist, wird der Besitzer automatisch auf den ausführenden Agent geändert.
- Der Besitzer lässt sich im Menü *Personen - Besitzer: Neuer Besitzer* in der Ticket-Detail-Ansicht manuell ändern. Das Ticket wird dann immer automatisch auch auf den neuen Besitzer gesperrt.
- Solange das Ticket nicht auf den Besitzer gesperrt ist, gibt es keine Einschränkungen für die Bearbeitung - dies kann also im Prinzip ignoriert werden.

## Ticket-Aktionen ##

### Ticket beobachten ###

- Mit der Ticket-Aktion *Beobachten* kann ein Agent alle Aktivitäten und den Status eines Tickets verfolgen.
- Mit einem Klick auf die Ticket-Aktion *Beobachten* in der Ticket-Detail-Ansicht wird das Ticket in die Beobachtungsliste aufgenommen (Symbol *Auge* erscheint).
- Bei einer neuen Aktion eines anderen Agenten bzw. eines Kunden in einem beobachteten Ticket, erscheint oben im Menü das Symbol *Auge mit Stern*.
- Eine Übersicht über alle beobachteten Tickets (*Meine beobachteten Tickets*), erhält man durch einen Klick auf das Symbol *Auge*.
- Zum Aufheben der Beobachtung muss für jedes Ticket die Ticket-Aktion *Nicht beobachten* in der Ticket-Detail-Ansicht durchgeführt werden.

### Ticket zusammenfassen ###

- Zwei Tickets können mit der Ticket-Aktion *Zusammenfassen* zu einem Ticket zusammengefasst werden.
- Das Zusammenfassen von Tickets bietet sich beispielsweise an, wenn ein Kunde ein Problem mehrmals meldet.
- Das Ursprungsticket bleibt im Ticket-Status *zusammengefasst* erhalten, Nachfragen auf das Ursprungsticket werden an das zusammengefasste Ticket angehängt.

### Ticket verknüpfen ###

- Wenn zwei Tickets in einem Zusammenhang stehen, aber nicht zusammengefasst werden können/sollen, ist die Ticket-Aktion *Verknüpfen* die richtige Wahl.
- Das Verknüpfen von Tickets bietet sich beispielsweise an, wenn Tickets miteinander verwandt sind, aber nicht zu einem Ticket zusammengefasst werden sollen/können.
- Die Verknüpfung wird in der Ticket-Detail-Ansicht am rechten Rand angezeigt.

### Ticket teilen ###

- Ein einzelner Artikel kann in ein neues Telefon-Ticket mit der Artikel-Aktion *Teilen* geteilt werden.
- Das Teilen von Tickets bietet sich beispielsweise an, wenn in einem Ticket zwei unterschiedliche Probleme beschrieben werden.
- Die Tickets werden automatisch miteinander verknüpft.

### Ticket verschieben ###

- Mit der Ticket-Aktion *Queue* kann ein Ticket in eine andere Queue verschoben werden.
- Das Verschieben von Tickets bietet sich beispielsweise an, wenn das Ticket nicht in den Zuständigkeitsbereich der Queue gehört und es eine andere passende OTRS-Queue gibt.

## Archivierung von Tickets #

- Im RRZE-Helpdesk (OTRS) werden *erfolgreich/erfolglos geschlossene* Tickets, die vor mehr als 3 Monaten (bis zum 30.11.2019: 6 Monaten) das letzte Mal geändert wurden, archiviert. Diese Tickets lassen sich nur noch über die Archivsuche finden (vgl. dazu FAQ-Artikel: [#42001611 Wie finde ich ältere Tickets wieder?](https://www.helpdesk.rrze.fau.de/otrs/public.pl?Action=PublicFAQZoom&ItemID=1611)).
- Die Archivierung dient primär dazu, aktuelle Tickets über die Suche schneller zu finden. Eine Archivsuche dauert deutlich länger und sollte deshalb, beispielsweise in Suchvorlagen, nicht standardmäßig aktiviert werden.
- Bitte beachten Sie, dass durch die Archivierung das Ticket automatisch aus Ihrer Beobachtungsliste entfernt wird (vgl. Ticket-Aktionen: Ticket beobachten).
