# Persönliche Einstellungen #

## Allgemeines ##

- OTRS kann durch die persönlichen Einstellungen an die Bedürfnisse und Vorlieben des Agenten angepasst werden.
- [Anzeigen/Ändern der Einstellungen](https://www.helpdesk.rrze.fau.de/otrs/index.pl?Action=AgentPreferences) ist über das Zahnrad-Symbol im oberen Menü (je nach Skin links oder rechts) möglich.

## Mögliche Einstellungen ##

- **Sprache:** Sprache der Benutzeroberfläche ändern
- **Abwesenheitszeit:** Abwesenheitszeiten pflegen
- **Persönlicher Namenszusatz (Adelstitel):** Persönlichen Namenszusatz pflegen (i. d. R. irrelevant)
- **Meine Queues:** Queue(s) abonnieren für Ticket-Übersichten und E-Mail-Benachrichtigungen
- **Ticket-Benachrichtigungen:** E-Mail-Benachrichtigungen ein-/ausschalten (bezieht sich immer auf alle unter *Meine Queues* abonnierten Queues)
- **Skin:** Oberflächen-Skin ändern
- **Aktualisierungszeiten der Übersichten:** Automatisches Neuladen bestimmter Seiten
- **Ansicht nach Ticket-Erstellung:** Verhalten nach der Ticket-Erstellung
