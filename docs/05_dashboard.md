# Übersicht (Dashboard) #

## Allgemeines ##

- Die sogenannte *Übersicht* besteht aus dem Hauptbereich und einer seitlichen Spalte (rechts).
- Die einzelnen Kästen in der Übersicht werden *Widgets* genannt und verfügen teilweise über eigene Einstellungen.
- Die genannten Bereiche der Übersicht können mit Widgets gefüllt und an persönliche Vorlieben angepasst werden.
- Zum Verschieben der Widgets kann Drag & Drop verwenden werden.
- Widget entfernen und Widget-Einstellungen (falls möglich):
  - Die Maus auf die graue Kopfzeile mit der Überschrift eines Widgets bewegen.
  - Es erscheint ein Zahnrad-Symbol für Einstellungen und ein *X*-Symbol zum Entfernen des Widgets
- Gelöschte Widgets können jederzeit über den Menüpunkt *Einstellungen* oben rechts in der seitlichen Spalte wieder in die Übersicht geholt werden.
- Die Reihenfolge der Menüpunkte (z. B. Übersicht, Tickets, FAQ, etc.) im oberen Menü kann ebenfalls durch Drag & Drop angepasst werden.

## Widgets in der Übersicht ##

- Durch Klick auf den Spaltenkopf in Widgets mit Ticket-Übersichten (z. B. *Neue Tickets*) kann die Sortierung angepasst werden.
- Reiter in den Widgets beachten:
  - **Meine gesperrten Tickets:** auf mich gesperrte Tickets
  - **Meine beobachteten Tickets:** von mir beobachtete Tickets
  - **Tickets in "Meine Queues":** Tickets in meinen abonnierten Queues (siehe *Persönliche Einstellungen*)
  - **Alle Tickets:** Tickets aller Queues (auf die ich Berechtigungen habe)
- Über das Trichter-Icon können Tickets nach bestimmten Kriterien in Widgets mit Ticket-Übersichten gefiltert werden (Achtung: Einstellung wird gespeichert).
- Wenn in Widgets mit Ticket-Übersichten zu viele Tickets vorhanden sind, werden diese auf mehreren Seiten (Pagination) angezeigt. Ein Seitenwechsel ist über die Seitenzahl bzw. Pfeilsymbole oben rechts möglich.
