# Hinweise zum Dokument #

Die Anleitung wurde im Rahmen von Erstanwenderschulungen für das Helpdesk-System an der FAU im Auftrag des Regionalen Rechenzentrums Erlangen (RRZE) erstellt.

## Autor ##

- Dominik Volkamer, [Regionales Rechenzentrum Erlangen (RRZE)](https://www.rrze.fau.de)

## Lizenz ##

Die Anleitung wird unter einer [Creativ Commons Lizenz](https://creativecommons.org/licenses/?lang=de) bereitgestellt:

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de)

- Namensnennung
- Nicht kommerziell
- Weitergabe unter gleichen Bedingungen

## Verwaltung und Bearbeitung der Anleitung ##

Die Anleitung wird in einem öffentlichen Repository auf der Plattform *GitLab* des Regionalen Rechenzentrum Erlangen (RRZE) bereitgestellt und gepflegt:

GitLab-URL: https://gitlab.rrze.fau.de/helpdesk/helpdesk-basics

## Fehler und Ergänzungen ##

Korrekturvorschläge können dem Autor gerne per [E-Mail](mailto:dominik.volkamer@fau.de) mitgeteilt werden. Ergänzungen werden, soweit dies zeitlich möglich ist, auf Wunsch umgesetzt.
