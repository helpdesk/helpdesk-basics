# Tickets #

## Neues Telefon-Ticket ##

- Ein Agent erstellt ein neues Telefon-Ticket, wenn er von einem Kunden angerufen wird und den Inhalt des Gesprächs in einem Ticket dokumentieren will.
- Der Kunde wird normalerweise nicht über das Erstellen des Tickets informiert (nur bei einer konfigurierten automatischen Antwort in der entsprechenden Queue).
- Zum Erstellen eines neuen Telefon-Tickets müssen mindestens die Pflichtfelder des Formulars ausgefüllt werden. Ein Klick auf *Erstellen* erzeugt das neue Ticket.

## Neues E-Mail-Ticket ##

- Ein Agent erstellt ein neues E-Mail-Ticket, wenn die Anfrage vom Agenten und nicht vom Kunden ausgeht und die Antwort des Kunden im Ticketsystem gespeichert werden soll.
- Bei einem neuen E-Mail-Ticket bekommt der Kunde immer eine E-Mail.
- Zum Erstellen eines neuen E-Mail-Tickets müssen mindestens die Pflichtfelder des Formulars ausgefüllt werden. Ein Klick auf *E-Mail übermitteln* erzeugt das neue Ticket.

## Ansicht nach Queues ##

- Standardmäßig werden Tickets aus allen abonnierten Queues angezeigt (*Meine Queues*, siehe *Persönliche Einstellungen*).
- Durch Klick auf eine Queue (übergeordnete Queue klappt sich aus und zeigt untergeordnete Queues an), kann eine spezielle Queue ausgewählt werden.
- Reiter beachten:
  - **Alle Tickets:** alle Tickets (inkl. auf andere Agenten gesperrte Tickets)
  - **Verfügbare Tickets:** nicht gesperrte Tickets (Standard)
- verschiedene Ansichten (Buchstaben-Symbole rechts oben) mit mehr oder weniger Information:
  - **S:** Tabellarische Form (Spalten über Zahnrad-Icon anpassbar)
  - **M:** Ausführlichere Form mit mehr Ticket-Meta-Informationen
  - **L:** Ausführlichste Form mit Ticket-Inhalten

## Ticket-Detail-Ansicht ##

- Die Ticket-Detail-Ansicht zeigt alle Meta-Informationen und Inhalte eines Tickets an.
- Die Überschrift zeigt die Priorität (farbiges Rechteck), die eindeutige Ticket-Nummer und den Ticket-Titel.

### Ticket-Aktionen ###

- **Zurück:** zur Übersicht zurückkehren
- **Sperren/Entsperren:** Ticket auf mich sperren bzw. für alle freigeben
- **Drucken:** PDF-Export zum Ausdrucken
- **Priorität:** Ticket-Priorität ändern
- **Personen:**
  - **Besitzer:** Ticket-Besitzer ändern, d. h. Ticket auf anderen Agenten sperren
  - **Kunde:** Kundenbenutzer ändern
- **Kommunikation:**
  - **Notiz:** Notiz an Ticket anhängen
  - **Ausgehender Telefonanruf:** Notiz über ausgehenden Telefonanruf an Ticket anhängen
  - **Eingehender Telefonanruf:** Notiz über eingehenden Telefonanruf an Ticket anhängen
- **Warten:** Ticket in den Wartezustand versetzen
- **Beobachten:** Ticket beobachten
- **Schließen:** Ticket schließen
- **Spam:** Ticket in die Spam-Queue verschieben und schließen
- **Verschiedenes:**
  - **Historie:** Historie eines Tickets anzeigen (alle Bearbeitungsschritte)
  - **Freie Felder:** dynamische Felder hinzufügen/ändern und Ticket-Status (z. B. offen, geschlossen) manuell ändern
  - **Verknüpfen:** Ticket mit anderem Ticket verknüpfen
  - **Zusammenfassen:** das aktuelle Ticket zu einem anderen Ticket (Ticketnummer benötigt) zusammenfassen
  - **MasterSlave:** MasterSlave-Ticket erstellen (wird eher selten benötigt)
- **Queue:** Ticket in andere Queue verschieben

### Artikelübersicht ###

- In der Artikelübersicht werden alle Artikel (z. B. ein- und ausgehenden Nachrichten, Notizen) aufgelistet.
- Mit einem Klick auf einen Artikel in der Artikelübersicht springt man zum entsprechenden Artikel.
- Modus der Ansicht kann über die zwei Strich-Symbole gewechselt werden:
  - **einen Artikel anzeigen:** Es wird immer nur der in der Artikelübersicht ausgewählte Artikel angezeigt.
  - **alle Artikel anzeigen:** Es werden alle Artikel angezeigt und durch Klicken auf einen Artikel in der Artikelübersicht scrollt man zu entsprechenden Artikel.
- Über das Trichter-Symbol können bestimmte Artikel gefiltert werden.

### Artikelaktionen ###

- **Markieren:** Artikel in der Artikelübersicht hervorheben
- **Unformatierte Ansicht:** zeigt den Quelltext der E-Mail an (nur bei ein- und ausgehenden E-Mails)
- **Drucken:** PDF-Export zum Ausdrucken eines einzelnen Artikels
- **Teilen:** neues Ticket mit diesem Artikel erstellen
- **Umleiten:** leitet den Artikel um (Bounce)
- **Weiterleiten:** leitet den Artikel weiter (Forward)
- **Antworten:** Antwort auf einen Artikel verfassen (Auswahl von Vorlage über Drop-Down-Menü)

### Randspalte ###

- **Ticket-Informationen:** alle Metadaten zu einem Ticket
- **Kundeninformation:** Informationen zum Kunden eines Tickets
  - Kundeninformationen werden aus dem IdentityManagement (IdM) der FAU bezogen.
  - Die Anzeige der Kundeninformation funktioniert nur dann, wenn der Kunde im IdM mit einer Person an der FAU verknüpft werden kann (z. B. Kunde sendet eine Anfrage von seiner "@fau.de"-E-Mail-Adresse).
- **Verknüpfte Objekte:** verknüpfte Tickets oder FAQ-Artikel

## Suchfunktion ##

- Geschlossene Tickets sind nur über die Suchfunktion (oder die URL) in der Oberfläche auffindbar.
- Zum Öffnen des Suchformulars auf das *Lupe*-Symbol im oberen Menü klicken.
- Es sind viele verschiedene Suchparameter möglich, z. B. Ticket-Nummer, Queue, Volltext.
- Je genauer die Suche eingegrenzt wird, desto schneller werden Ergebnisse gefunden.
- Erweitern der Suchparameter ist über *Ein weiteres Attribut hinzufügen* und einem Klick auf das *Plus*-Symbol möglich.
- Die Archivsuche ist für Tickets, die vor 6 Monaten oder länger das letzte Mal bearbeitet wurden (Achtung: Suche dauert deutlich länger).
- Das Speichern von Suchparametern und Werten für häufig ähnliche Suchen ist über Suchvorlagen möglich.
  - Zunächst alle Suchparameter und Werte wie gewünscht konfigurieren.
  - Anschließend auf *Suchvorlage-Neue anlegen* klicken, Namen für die Suche vergeben und *Hinzufügen* klicken.
- In Ergebnisliste der Suche lassen sich die Suchparameter und Werte über *Suchoptionen ändern* nachträglich anpassen (nicht über das *Lupe*-Symbol).
