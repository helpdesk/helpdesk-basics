# Wichtige Begriffe #

## OTRS ##

- OTRS ist ein Ticketsystem und dient zur Erfassung, Speicherung und Bearbeitung von Kundenanfragen.
- Die Abkürzung *OTRS* steht für *Open Technology Real Services* (früher für *Open Ticket Request System*).
- OTRS steht als freie Software unter der Lizenz *GNU General Public License* (GPL), d. h. der Quelltext ist frei verfügbar und kann angepasst und erweitert werden.
- OTRS ist ein weitverbreitetes System im Unternehmensumfeld und wird von vielen großen Unternehmen zur Kundenkommunikation eingesetzt.
- Das Ticketsystem ist eine Webanwendung, d. h. es wird über den Browser (z. B. Firefox, Chrome) bedient und erfordert keine Installation für die Nutzer des Systems auf dem PC oder mobilen Gerät (plattform- und betriebssystemunabhängig).

## Kunde ##

- Ein Kunde ist im Kontext eines Ticketsystems (vereinfacht ausgedrückt) der Ersteller einer Anfrage.
- Der Kunde weiß normalerweise nicht, dass er mit einem Ticketsystem korrespondiert.
- Der Kunde kann sich nicht im Ticketsystem einloggen, ein Kundenportal steht für den RRZE-Helpdesk (OTRS) nicht mehr zur Verfügung.

## Agent ##

- Ein Agent bearbeitet Anfragen (Tickets) und Einträge in der Wissensdatenbank (FAQs) im Ticketsystem.
- Um als Agent mit dem Ticketsystem zu arbeiten, ist ein Login über die Weboberfläche am [RRZE-Helpdesk (OTRS)](https://www.helpdesk.rrze.fau.de) notwendig.
- Ein Zugang zum RRZE-Helpdesk (OTRS) wird nicht automatisch vergeben, sondern muss gesondert beantragt werden (siehe [OTRS-Zugang](https://www.anleitungen.rrze.fau.de/otrs-ticketsystem/otrs-zugang/)).

## Ticket und Artikel ##

- Ein Ticket bündelt die gesamte Kommunikation einer Kundenanfrage.
- Jedes Ticket erhält eine eindeutige Ticketnummer, über die das Ticket jederzeit wiedergefunden werden kann.
- Tickets beinhalten i. d. R. mehrere Artikel, die vom System chronologisch durchnummeriert und in der Artikelübersicht eines Tickets angezeigt werden.
- Ein Artikel entspricht einer ein- oder ausgehenden E-Mail oder einem Bearbeitungsschritt (z. B. einer Notiz) in einem Ticket.

## Notiz ##

- Zur internen Kommunikation zwischen Agenten werden im Ticketsystem sogenannte Notizen verwendet.
- Notizen bieten sich an, um beispielsweise anderen Agenten den aktuellen Stand des Tickets mitzuteilen.
- Viele Ticket-Aktionen hängen automatisch eine Notiz an das Ticket an, in der die Aktion und der Grund dieser genauer beschrieben werden kann.

## Queue ##

- Eine sogenannte Queue ist ein Postfach bzw. eine Warteschlange für Tickets abgegrenzter Bereiche.
- Ein Ticket ist immer genau einer Queue zugeordnet.
- Queues sind ineinander verschachtelbar, jeweils getrennt durch zwei Doppelpunkte (z. B. *RRZE::OTRS-Support*).
- Tickets können zwischen verschiedenen Queues verschoben werden.
- Pro Queue können je eine Anrede und eine Signatur sowie mehrere Vorlagen und automatische Antworten konfiguriert werden.
- Berechtigungen für Agenten werden auf Basis von Queues vergeben.
- Es können eine oder mehrere eingehende E-Mail-Adresse/n (Funktionsadresse/n) je Queue konfiguriert werden. Jede Queue hat genau eine Absenderadresse (Funktionsadresse).

## Anrede und Signatur ##

- Pro Queue ist je eine Anrede und eine Signatur konfigurierbar.
- Bei jeder Antwort an den Kunden wird die Anrede und Signatur automatisch eingefügt und Variablen (z. B. der Name des Kunden, der Name des Agenten) eingesetzt.
- Die Anrede/Signatur einer Queue wird vom OTRS-Support bzw. von einem Agent mit der [Mini-Admin-Rolle](https://www.anleitungen.rrze.fau.de/otrs-ticketsystem/mini-admin-rolle/) (nur Signatur) gepflegt.

## Vorlage und automatische Antwort ##

- Eine Vorlage dient zum Antworten auf Tickets des Kunden oder zum Erstellen von neuen Telefon- oder E-Mail-Tickets (s. u.).
- In Vorlagen können beispielsweise häufig verwendete Antworttexte hinterlegt und einfach wiederverwendet werden.
- Eine automatische Antwort dient zur automatischen Benachrichtigung des Kunden, z. B. um den Erhalt der Anfrage zu bestätigen.
- Es gibt verschiedene Typen von automatischen Antworten, z. B. bei der Erstellung eines neuen Tickets oder bei einer Nachfrage des Kunden.
- Vorlagen und automatische Antworten werden vom OTRS-Support bzw. von einem Agent mit der [Mini-Admin-Rolle](https://www.anleitungen.rrze.fau.de/otrs-ticketsystem/mini-admin-rolle/) gepflegt.

## Historie ##

- OTRS ist revisionssicher, d. h. alle Aktionen an einem Ticket werden in einer Historie gespeichert und bleiben nachvollziehbar.
- Die Historie kann über die Ticket-Aktion *Verschiedenes-Historie* in der Ticket-Detail-Ansicht für jedes Ticket aufgerufen werden.
- Das Löschen von Tickets ist für Agenten nicht möglich (nur über den OTRS-Support). Viele Ticket-Aktionen lassen sich nicht rückgängig machen oder können nicht nachträglich geändert werden.

## FAQ ##

- Der RRZE-Helpdesk (OTRS) stellt das sogenannte FAQ-Modul bereit. Hierbei handelt es sich um eine ins Ticketsystem integrierte Wissensdatenbank.
- In der Wissensdatenbank können häufig gestellte Fragen (FAQ = Frequently Asked Questions) strukturiert abgelegt werden.
- Es ist möglich, FAQ-Artikel als *intern* (nur für andere Agenten des selben Bereichs) oder als *öffentlich* (weltweit abrufbar) zu markieren.
