# Grundlagen des Helpdesk-Systems (OTRS) #

Grundlagenanleitung zur Arbeit mit dem Helpdesk-System *OTRS* an der FAU

## Autor ##

- Dominik Volkamer, [Regionales Rechenzentrum Erlangen (RRZE)](https://www.rrze.fau.de)

## Downloads ##

Diese Anleitung wird im [GitLab-Projekt 'helpdesk-basics'](https://gitlab.rrze.fau.de/helpdesk/helpdesk-basics) gepflegt. Hier können die Quelldateien und Exportversionen für verschiedene Dateiformate geladen werden:

- Quelldateien (Markdown): [Verzeichnis 'docs/'](docs/)
- Exportformate:
  - [Markdown](export/helpdesk-basics.md)
  - [PDF](export/helpdesk-basics.pdf)

## Lizenz ##

Die Anleitung wird unter einer [Creativ Commons Lizenz](https://creativecommons.org/licenses/?lang=de) bereitgestellt:

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.de)

- Namensnennung
- Nicht kommerziell
- Weitergabe unter gleichen Bedingungen

## Verwaltung und Bearbeitung der Anleitung ##

### Voraussetzung ###

- [Pandoc](https://pandoc.org/)

### Verzeichnisstruktur ###

- [docs/](docs/): Quelldateien im Markdown-Format (pro Kapitel eine Datei)
- [export/](export/): Generierte Exportdateien
- [metadata/](metadata/): Metadaten für die Generierung der Exporte mit *Pandoc*
- [scripts/](scripts/): Skripte zum Generieren der Exporte
- [templates/](templates/): Templates für die Generierung der Exporte mit *Pandoc*

### Bearbeitung ###

Änderungen in der Anleitung erfolgen ausschließlich in den Quelldateien im Verzeichnis [docs/](docs/). Anschließend können die Exportdateien mit dem Bash-Skript `scripts/generate_export.sh` unter Linux neu generiert werden.
